<?php

namespace app\models;

use app\core\DbModel;
use app\core\Model;

class Blog extends DbModel
{
	public string $title = '';
	public string $description = '';
	public string $created_at = '';

	public static function tableName(): string
	{
		return 'blog';
	}

	public function rules(): array
	{
		return [
			'title' => [self::RULE_REQUIRED],
			'description' => [self::RULE_REQUIRED],
		];
	}
	public static function primaryKey(): string
	{
		return 'id';
	}

	public function attributes(): array
	{
		return [
			'title',
			'description',
			'created_at'
		];
	}

	public function getAll() {

	}
}