<?php

require_once __DIR__ . '/vendor/autoload.php';

use app\core\Application;
use app\controllers\HomeController;
use app\controllers\AuthController;
use app\models\User;

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();
$dbconfig = [
	'userClass' => User::class,
	'db' => [
		'dsn' => $_ENV['DB_DSN'],
		'user' => $_ENV['DB_USER'],
		'password' => $_ENV['DB_PASSWORD'],
	]
];

$app = new Application(dirname(__DIR__), $dbconfig);

$app->db->install();