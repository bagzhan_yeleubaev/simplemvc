<?php

namespace app\controllers;

use app\core\Application;
use app\core\Controller;
use app\core\Request;
use app\core\Response;
use app\models\LoginForm;
use app\models\User;

class AuthController extends Controller
{

	public function login(Request $request, Response $response)
	{
		$loginForm = new LoginForm();
		$params = [
			'model' => $loginForm
		];
		return $this->render('login', $params);
	}

	public function authorize(Request $request, Response $response)
	{
		$loginForm = new LoginForm();
		$loginForm->loadData($request->all());
		if ($loginForm->validate() && $loginForm->login())
		{
			return $response->redirect('/');
		}
	}

	public function register(Request $request, Response $response)
	{
		$user = new User();
		$params = [
			'model' => $user
		];
		return $this->render('register', $params);
	}

	public function registerUser(Request $request, Response $response)
	{
		$user = new User();
		$user->loadData($request->all());

		if ($user->validate() && $user->save())
		{
			Application::$app->session->setFlash('success', 'Успешная регистрация!');
			$response->redirect('/');
		}
		$params = [
			'model' => $user
		];
		return $this->render('register', $params);

	}

	public function logout(Request $request, Response $response)
	{
		Application::$app->logout();
		$response->redirect('/');
	}

}