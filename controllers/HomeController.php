<?php

namespace app\controllers;

use app\core\Application;
use app\core\Controller;
use app\core\middlewares\AuthMiddleware;
use app\core\Request;
use app\core\Response;
use app\models\Blog;
use app\models\Comment;

class HomeController extends Controller
{
	public function __construct()
	{
		$this->registerMiddleware(new AuthMiddleware());
	}

	public function index()
	{
		$blog = Blog::where(['id' => 1]);
		$comments = Comment::all();
		$commentModel = new Comment();
		foreach ($comments as $comment)
		{
			$comment->username = Comment::getUserName($comment->user_id);
			$comment->email = Comment::getUserEmail($comment->user_id);
		}
		$params = [
			'blog' => $blog,
			'comments' => $comments,
			'model' => $commentModel
		];
		return $this->render('blog', $params);
	}

	public function comment(Request $request, Response $response)
	{

		$comment = new Comment();
		$comment->loadData($request->all());

		if ($comment->validate() && $comment->save())
		{
			Application::$app->session->setFlash('success', 'Комментарий успешно добавлен!');
			$response->redirect('/');
		}
		$blog = Blog::where(['id' => 1]);
		$comments = Comment::all();
		foreach ($comments as $com)
		{
			$com->username = Comment::getUserName($com->user_id);
			$com->email = Comment::getUserEmail($com->user_id);
		}
		$params = [
			'blog' => $blog,
			'comments' => $comments,
			'model' => $comment
		];
		return $this->render('blog', $params);
	}
}