#!/bin/bash

if [ ! -f "vendor/autoload.php" ]; then
    echo "composer not installed"
    composer install
else
    echo "composer installed"
fi


php Install.php
exec "$@"

