FROM php:8.1-fpm

RUN apt-get update \
    && apt-get install -y unzip libpq-dev libcurl4-gnutls-dev libzip-dev \
    && docker-php-ext-install pdo pdo_mysql zip

WORKDIR /var/www/tz
COPY . .

COPY --from=composer:2.2.6 /usr/bin/composer /usr/bin/composer
CMD [ "docker/entrypoint.sh" ,"php-fpm","-F"]