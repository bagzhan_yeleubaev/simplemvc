#SIMPLE MVC

In order to setup project you should have `Docker` installed

`PROJECT SETUP`
```
docker-compose up --build -d
```

`Launch Project`

Once you built project go to

```
 http://localhost:8098
```
