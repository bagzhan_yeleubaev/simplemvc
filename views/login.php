<h1>Логин</h1>
<form action="/login" method="post">
    <div class="mb-3">
        <label for="email" class="form-label">Email</label>
        <input type="email" class="form-control<?php echo $model->hasError('email') ? ' is-invalid' : '' ?>" value="<?php echo $model->email ?>" id="email" name="email" placeholder="name@example.com">
        <div class="invalid-feedback">
			<?php echo $model->getFirstError('email'); ?>
        </div>
    </div>
    <div class="mb-3">
        <label for="password" class="form-label">Пароль</label>
        <input type="password" class="form-control<?php echo $model->hasError('password') ? ' is-invalid' : '' ?>"  name="password" id="password"></input>
        <div class="invalid-feedback">
			<?php echo $model->getFirstError('password'); ?>
        </div>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>