<div class="content">
    <h1> <?php echo $blog->title; ?></h1>
    <div class="mb-3">
		<?php echo $blog->description ?>
    </div>
    <form class="mb-3" action="/comment" method="post" id="commentForm">
        <label for="title" class="form-label darkgrey">Заголовок</label>
        <input type="text" class="form-control<?php echo $model->hasError('title') ? ' is-invalid' : '' ?>" value="<?php echo $model->title ?>" id="title" name="title"></input>
        <div class="invalid-feedback">
			<?php echo $model->getFirstError('title'); ?>
        </div>
        <label for="description" class="form-label darkgrey <?php echo $model->hasError('description') ? ' is-invalid' : '' ?>" value="<?php echo $model->description ?>">Комментарий</label>
        <textarea class="form-control" id="description" name="description" rows="3"></textarea>
        <div class="invalid-feedback">
			<?php echo $model->getFirstError('description'); ?>
        </div>
        <br>
        <button type="submit" id="mybtn" class="btn btn-primary">Отправить</button>
    </form>
</div>

<div class="comments">
	<?php foreach ($comments as $comment): ?>
        <div class="comment">
            <div class="comment-header">
                <img src="profile.png" width="50" alt="">
            </div>
            <div class="comment-body">
                <div class="comment-autor">
					<?php echo $comment->username; ?>
                </div>
                <div class="comment-data">
                    <a href="mailto:<?php echo $comment->email ?>" class="comment-email"> <?php echo $comment->email ?></a>
                    <div class="comment-date justify-contend-end">
						<?php echo $comment->created_at; ?>
                    </div>
                </div>
                <div class="comment-description">
                    <span class="comment-title">
                        <?php echo $comment->title; ?>
                    </span>
                    <span class="comment-text"><?php echo $comment->description; ?></span>
                </div>
            </div>
        </div>
	<?php endforeach; ?>
</div>
