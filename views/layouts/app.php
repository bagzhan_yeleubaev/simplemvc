<?php
 use app\core\Application;
?>

<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Тестовое задание</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <link rel="stylesheet" href="main.css">
</head>
<body>
<nav class="navbar navbar-expand-lg ">
    <div class="container-fluid">
        <a class="navbar-brand" href="/"> <img src="favicon.ico" width="50px" alt="Блог"> <span>РОГА И  КОПЫТА</span>  </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class=" justify-content-end" id="navbarSupportedContent">

			<?php if (Application::isGuest()): ?>
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="/login">Логин</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="/register">Регистрация</a>
                    </li>
                </ul>
			<?php else: ?>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <div class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
						<?php echo Application::$app->user->getDisplayName()?>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="/logout">Выйти</a></li>

                    </ul>
                </div>
            </div>
			<?php endif ?>
        </div>
    </div>
</nav>
<div class="container">
	<?php if(Application::$app->session->getFlash('success')): ?>
        <div class="alert alert-success">
			<?php echo Application::$app->session->getFlash('success');?>
        </div>
	<?php endif; ?>
    {{content}}
</div>

</body>
<footer class="d-flex flex-wrap justify-content-between align-items-center py-3 my-0 border">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>
    <div class="col-md-4 d-flex align-items-center">
        <a href="/" class="mb-3 me-2 mb-md-0 text-muted text-decoration-none lh-1">
            <svg class="bi" width="30" height="24"><use xlink:href="#bootstrap"></use></svg>
        </a>
        <span class="mb-3 mb-md-0 text-muted">© 2022 Company, Inc</span>
    </div>

    <ul class="nav col-md-4 justify-content-center list-unstyled d-flex">
        <li class="ms-3"><a class="text-muted" href="https://www.instagram.com/instagram/" target="_blank"><img src="insta.png" width="24" alt=""></a></li>
        <li class="ms-3"><a class="text-muted" href="https://twitter.com"><img src="twitter.svg" width="24" alt=""></a></li>
        <li class="ms-3"><a class="text-muted" href="https://www.facebook.com/facebook/"><img src="facebook.png" width="24" alt=""></a></li>
    </ul>
</footer>
</html>