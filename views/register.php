<h1>Регистрация</h1>
<form action="/register" method="post">
    <div class="row">
        <div class="col">
            <label for="firstname" class="form-label">Имя</label>
            <input type="text" class="form-control<?php echo $model->hasError('firstname') ? ' is-invalid' : '' ?>" value="<?php echo $model->firstname ?>" id="firstname" name="firstname" placeholder="Саша">
            <div class="invalid-feedback">
                <?php echo $model->getFirstError('firstname'); ?>
            </div>
        </div>
        <div class="col">
            <label for="lastname" class="form-label">Фамилия</label>
            <input type="text" class="form-control<?php echo $model->hasError('lastname') ? ' is-invalid' : '' ?>" value="<?php echo $model->lastname ?>" id="lastname" name="lastname" placeholder="Петров">
            <div class="invalid-feedback">
				<?php echo $model->getFirstError('lastname'); ?>
            </div>
        </div>
    </div>
    <div class="mb-3">
        <label for="email" class="form-label">Email</label>
        <input type="email" class="form-control<?php echo $model->hasError('email') ? ' is-invalid' : '' ?>" value="<?php echo $model->email ?>" id="email" name="email" placeholder="name@example.com">
        <div class="invalid-feedback">
			<?php echo $model->getFirstError('email'); ?>
        </div>
    </div>
    <div class="mb-3">
        <label for="password" class="form-label">Пароль</label>
        <input type="password" class="form-control<?php echo $model->hasError('password') ? ' is-invalid' : '' ?>"  name="password" id="password"></input>
        <div class="invalid-feedback">
			<?php echo $model->getFirstError('password'); ?>
        </div>
    </div>
    <div class="mb-3">
        <label for="confirmPassword" class="form-label">Повторите пароль</label>
        <input type="password" class="form-control<?php echo $model->hasError('confirmPassword') ? ' is-invalid' : '' ?>" name="confirmPassword" id="confirmPassword"></input>
        <div class="invalid-feedback">
			<?php echo $model->getFirstError('confirmPassword'); ?>
        </div>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>