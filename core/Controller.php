<?php

namespace app\core;

use app\core\middlewares\BaseMiddleware;

class Controller
{
	public string $layout = 'app';
	public string $action = '';
	protected array $middlewares = [];
	public function setLayout() {
		$this->layout = 'auth';
	}
	public function render($view, $params = []) {
		return Application::$app->router->renderView($view, $params);
	}

	public function registerMiddleware(BaseMiddleware $middleware) {
		$this->middlewares[] = $middleware;
	}

	public function getMiddlewares() {
		return $this->middlewares;
	}

}