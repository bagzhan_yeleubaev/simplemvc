<?php

namespace app\core;

class Database
{
	public \PDO $pdo;

	public function __construct(array $config)
	{
		$dsn = $config['dsn'] ?? '';
		$user = $config['user'] ?? '';
		$password = $config['password'] ?? '';
		$this->pdo = new \PDO($dsn, $user, $password);
		$this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
	}

	public function install()
	{
		$this->pdo->exec("CREATE TABLE IF NOT EXISTS users (
			id INT AUTO_INCREMENT PRIMARY KEY,
			firstname VARCHAR(100) not null, 
			lastname VARCHAR(100) not null,
			email VARCHAR(200) not null,
			password VARCHAR(200) not null,
			created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
		)");

		$this->pdo->exec("CREATE TABLE IF NOT EXISTS blog (
			id INT AUTO_INCREMENT PRIMARY KEY,
			title VARCHAR(255) not null,
			description LONGTEXT not null,
			created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
		)");
		$this->pdo->exec("INSERT INTO blog(title, description) VALUES ('Humans of New York', 'Humans of New York (HONY) – один из тех блогов о фотографии и путешествиях, которые обрели популярность благодаря Facebook.

			Этот блог, посвящённый повседневной жизни обычных людей, очень серьёзно относится к пользовательскому опыту.
			
			Когда посетитель попадает на главную страницу HONY, он может выбрать одну из трёх основных категорий блога: истории, страны или сериалы.
			
			Это просто, но эффективно и позволяет пользователю сразу перейти к тому контенту, который ему интересен больше всего.
			
			Таким образом, пользователям удобно знакомиться с сайтом. Подумайте о создании нескольких основных категорий или тщательно подобранных списков, чтобы придать структуру вашему блогу и упростить пользовательский опыт.
			
			Лучшие идеи для заимствования: категории или списки актуального содержания.')");
		$this->pdo->exec("CREATE TABLE IF NOT EXISTS comments (
			id INT AUTO_INCREMENT PRIMARY KEY,
			user_id INT,
			blog_id INT,
			title VARCHAR(255) not null,
			description VARCHAR(1000) not null,
			created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
			FOREIGN KEY (user_id) REFERENCES users(id),
			FOREIGN KEY (blog_id) REFERENCES blog(id)
		)");
	}

	public function prepare($sql)
	{
		return $this->pdo->prepare($sql);
	}

}