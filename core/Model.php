<?php

namespace app\core;

abstract class Model
{
	public const RULE_REQUIRED = 'required';
	public const RULE_EMAIL = 'email';
	public const RULE_MIN = 'min';
	public const RULE_MAX = 'max';
	public const RULE_UNIQUE = 'unique';
	public const RULE_MATCH = 'match';


	public function loadData($data) {
		foreach ($data as $key => $value) {
			if(property_exists($this, $key)) {
				$this->{$key} = $value;
			}
		}
	}

//	public function toArray($data) {
//		foreach ($data as $key => val)
//	}
	abstract public function rules() : array;

	public array $errors = [];

	public function validate() {
		foreach ($this->rules() as $attribute => $rules ) {
			$value = $this->{$attribute};
			foreach ($rules as $rule) {
				$ruleName = $rule;
				if(!is_string($ruleName)) {
					$ruleName = $rule[0];
				}
				if($ruleName === self::RULE_REQUIRED && !$value) {
					$this->addErrorForRule($attribute, self::RULE_REQUIRED);
				}
				if ($ruleName === self::RULE_EMAIL && !filter_var($value, FILTER_VALIDATE_EMAIL)) {
					$this->addErrorForRule($attribute, self::RULE_EMAIL);
				}
				if ($ruleName === self::RULE_MIN && strlen($value) < $rule['min']) {
					$this->addErrorForRule($attribute, self::RULE_MIN, $rule);
				}
				if ($ruleName === self::RULE_MAX && strlen($value) > $rule['max']) {
					$this->addErrorForRule($attribute, self::RULE_MAX, $rule);
				}
				if($ruleName === self::RULE_MATCH && $value !== $this->{$rule['match']}) {
					$this->addErrorForRule($attribute, self::RULE_MATCH, $rule);
				}
				if($ruleName === self::RULE_UNIQUE) {
					$className = $rule['class'];
					$uniqueAttribute = $rule['attribute'] ?? $attribute;
					$tableName = $className::tableName();
					$statement = Application::$app->db->prepare("SELECT * FROM $tableName WHERE $uniqueAttribute = :$attribute");
					$statement->bindValue(":$uniqueAttribute", $value);
					$statement->execute();
					$record = $statement->fetchObject();
					if($record) {
						$this->addErrorForRule($attribute, self::RULE_UNIQUE, ['field' => $attribute]);
					}
				}
			}
		}
		return empty($this->errors);
	}

	private function addErrorForRule(string $attribute, string $rule, $params = [])
	{
		$message = $this->errorMessages()[$rule] ?? '';
		foreach ($params as $key => $value) {
			$message = str_replace("{{$key}}", $value, $message);
		}
		$this->errors[$attribute][] = $message;
	}

	public function addError(string $attribute, string $message)
	{
		$this->errors[$attribute][] = $message;
	}

	public function errorMessages() {
		return [
			self::RULE_REQUIRED => 'Это поле обязательно для заполнения',
			self::RULE_EMAIL => 'Это поле должно быть корректным email адресом',
			self::RULE_MIN => 'Это поле должно содержать минимум {min} символов',
			self::RULE_MAX => 'Это поле должно содержать максимум {max} символов',
			self::RULE_MATCH => 'Это поле должно совпадать c {match}',
			self::RULE_UNIQUE => '{field} уже занято'
		];
	}

	public function hasError($attribure) {
		return $this->errors[$attribure] ?? false;
	}

	public function getFirstError($attibute) {
		return $this->errors[$attibute][0] ?? false;
	}
}